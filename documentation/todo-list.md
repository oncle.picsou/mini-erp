# Todo list

This document contains the todo list (or in other terms all the possible improvements), because there is a not a
common repository with its issues or user stories.

## List

### Backend

- [ ] Add namespace and remove the actual import, for example this `include __DIR__ . "/../validators/ValidatorX.php";`
- [ ] Maybe found unique way to do the query to the database and wrapper in a tool file.
- [ ] Create a "return code error" in order to improve for witch way the backend has returned an error 
(check with security before).
- [ ] Add "hide" configuration in order don't put on the code password and sensible data but put it in another way,
this task is for the staging and production.
- [ ] Wrapper the validation (like validation for id) and to use the constants.
- [ ] See if there is an integrated on Phalcon for production way
- [ ] To see composer.json
- [ ] To see if it is better to use a singleton for the Root-Utility
(maybe yes, maybe no for soma compatible with Phalcon).
- [ ] Validator:
  - [ ] See if it is possible to use the [Phalcon validator](https://docs.phalcon.io/4.0/it-it/validation).
  - [ ] Improved the validation controller like for instance the user and password (to have an user like this
      123!"£$ maybe it is not a so nice).
  - [ ] A person cannot bor after to have worked in the company
- There is a problem about the time-zone for the transactions.


### Frontend
- [ ] Found another compiler in order to avoid the problem about `if(!!variable)` and unction with types like
`add(name: string): void{[...]}`
- [ ] Improve the home page maybe with a random page.
- [ ] Put a foot bar.
- [ ] Improve the error alert when the user had edited a row and the server return with an error.
- [ ] Create a pagination system for the table with a caching and 
in order don't stress to match the network uses a hash system.
- [ ] Dashboard for the managing of the user and remove the home ironic phrase (putting there for the demo).
- [ ] Improve when it is on mobile way more in particular:
  - [ ] Navbar
- [ ] Remove the two warning on the front.
- [ ] To see the law the blind people about the site (in order to avoid any law risk and fine).
- [ ] Put pick-data and number input instead of text insertion.
- [ ] Put only the products of specific company during a transaction: before to choose the company and after the products
of that company. The same thing for the employee. In other terms, the user has to chose in this order: `company -> 
[employee and products]`.
- [ ] Not in the trace:
  - [ ] In order to be coherent when the company has sold some products to the clients the company balance must be increased?


### Common
- [ ] To do first transaction from provider to company.
- [ ] To implement the others tables and relations
- [ ] Tests
