# Evaluation Assignment-2021-v2

> The link works with [Typora](https://typora.io/) not on Gitlab
> (for Gitlab remove the dot form `./documentation/todo-list.md` to `/documentation/todo-list.md`)

- [Todo list](./documentation/todo-list.md)
  - [List](./documentation/todo-list.md#list)
  - [Backend](./documentation/todo-list.md#backend)
  - [Frontend](./documentation/todo-list.md#frontend)
  - [Common-end](./documentation/todo-list.md#common-end)
- [Links](./documentation/links.md)
   - [Backend](./documentation/todo-list.md#backend)
   - [Frontend](./documentation/todo-list.md#frontend)
   - [Backend and frontend](./documentation/todo-list.md#backend-and-frontend)


## Project

The project is not finished, it contains:
- Simple database in order to save, retrieve and edit the main tables,
plus a part of the transaction between the company and the client with
the frontend and backend code for doing the precedents operations (save, retrieve, edit and transaction).
It is possible to use the phalcon migration or
the [sql script](./scripts/version-db-100/Database.sql) for creating the database.
The final database could be [Database.sql](./scripts/version-db-100/Database.sql).

- Inside `mocha-fronten-test` there is skeleton for the tests with Mocha.


## Backend
- Version: Phalcon 4.1.2
- Php 7.4

## Front
- VueJs 2.6 + others library

## Run

In order to run in easily way, it is possible to use `erp.sh script`
> The erp.sh script works properly if the phalcon has been well installed
(in other words if the command `phalcon` can be launched by terminal). In order to launch by terminal a possible way is
to put on `zbash` file an alias like this (add this row on  `zbash`): alias phalcon='/Users/fxr/phalcon-devtools/phalcon'
If the computer doesn't use `zbash`,
it is convenient to modify the script in order to get the right configuration of the bash.

More in details:
- The backend part uses this ports: 8000 on localhost
- The frontend part uses this port: 8080 on localhost 

The fronted uses a [proxy]('./fronted/vue.config.js'):
```
module.exports = {
    devServer: {
        proxy: {
            'resource': {
                target: 'http://localhost:8000/',
            }
        }
    }
}
```
for redirect the request on the backend part.

