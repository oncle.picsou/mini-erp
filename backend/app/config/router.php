<?php

use Phalcon\Mvc\Dispatcher;

$router = $di->getRouter();

/***********************************************************************************************************************
 * Define the routes here
 **********************************************************************************************************************/

/*******************************************************************************************
 * Managing user
 */

/** Login **/
$router->add('/resource/login-user', array(
    'controller' => 'loginUser',
    'action' => 'loginUser'
))->via([
    'POST'
]);

/** Logout **/
$router->add('/resource/logout-user', array(
    'controller' => 'logoutUser',
    'action' => 'logoutUser'
))->via([
    'POST'
]);

/** Registration **/
$router->add('/resource/registration-user', array(
    'controller' => 'registrationUser',
    'action' => 'registrationUser'
))->via([
    'POST'
]);

/*******************************************************************************************
 * Tables
 */


/**********************************************
 * Single table
 */

/** Client Table **/
$router->add('/resource/client', array(
    'controller' => 'getClient',
    'action' => 'getClient'
))->via([
    'GET'
]);

$router->add('/resource/client', array(
    'controller' => 'postClient',
    'action' => 'postClient'
))->via([
    'POST'
]);

$router->add('/resource/client', array(
    'controller' => 'putClient',
    'action' => 'putClient'
))->via([
    'PUT'
]);

$router->add('/resource/client/{id}', array(
    'controller' => 'deleteClient',
    'action' => 'deleteClient',
    'id'  => 1
))->via([
    'DELETE'
]);


/** Company Table **/
$router->add('/resource/company', array(
    'controller' => 'getCompany',
    'action' => 'getCompany'
))->via([
    'GET'
]);

$router->add('/resource/company', array(
    'controller' => 'postCompany',
    'action' => 'postCompany'
))->via([
    'POST'
]);

$router->add('/resource/company', array(
    'controller' => 'putCompany',
    'action' => 'putCompany'
))->via([
    'PUT'
]);

$router->add('/resource/company/{id}', array(
    'controller' => 'deleteCompany',
    'action' => 'deleteCompany',
    'id'  => 1
))->via([
    'DELETE'
]);

/** Employee Table **/
$router->add('/resource/employee', array(
    'controller' => 'getEmployee',
    'action' => 'getEmployee'
))->via([
    'GET'
]);

$router->add('/resource/employee', array(
    'controller' => 'postEmployee',
    'action' => 'postEmployee'
))->via([
    'POST'
]);

$router->add('/resource/employee', array(
    'controller' => 'putEmployee',
    'action' => 'putEmployee'
))->via([
    'PUT'
]);

$router->add('/resource/employee/{id}', array(
    'controller' => 'deleteEmployee',
    'action' => 'deleteEmployee',
    'id'  => 1
))->via([
    'DELETE'
]);

/** Product Table **/
$router->add('/resource/product', array(
    'controller' => 'getProduct',
    'action' => 'getProduct'
))->via([
    'GET'
]);

$router->add('/resource/product', array(
    'controller' => 'postProduct',
    'action' => 'postProduct'
))->via([
    'POST'
]);

$router->add('/resource/product', array(
    'controller' => 'putProduct',
    'action' => 'putProduct'
))->via([
    'PUT'
]);

$router->add('/resource/product/{id}', array(
    'controller' => 'deleteProduct',
    'action' => 'deleteProduct',
    'id'  => 1
))->via([
    'DELETE'
]);

/** Provider Table **/
$router->add('/resource/provider', array(
    'controller' => 'getProvider',
    'action' => 'getProvider'
))->via([
    'GET'
]);

$router->add('/resource/provider', array(
    'controller' => 'postProvider',
    'action' => 'postProvider'
))->via([
    'POST'
]);

$router->add('/resource/provider', array(
    'controller' => 'putProvider',
    'action' => 'putProvider'
))->via([
    'PUT'
]);

$router->add('/resource/provider/{id}', array(
    'controller' => 'deleteProvider',
    'action' => 'deleteProvider',
    'id'  => 1
))->via([
    'DELETE'
]);


/**********************************************
 * Transaction tables
 */

/** companyClient **/
$router->add('/resource/companyClient', array(
    'controller' => 'getCompanyClient',
    'action' => 'getCompanyClient',
))->via([
    'GET'
]);

$router->add('/resource/companyClient', array(
    'controller' => 'postCompanyClient',
    'action' => 'postCompanyClient',
))->via([
    'POST'
]);

/*
Not used for the moment.
$router->add('/resource/companyClient', array(
    'controller' => 'getCompanyClient',
    'action' => 'putCompanyClient',
))->via([
    'PUT'
]);
*/
$router->add('/resource/companyClient/{id}', array(
    'controller' => 'deleteCompanyClient',
    'action' => 'deleteCompanyClient',
    'id'  => 1
))->via([
    'DELETE'
]);

/*******************************************************************************************
 * Default router
 */

// ERROR 404 - Page not found
$di->set('dispatcher', function () use ($di) {
    $evManager = $di->getShared('eventsManager');
    $evManager->attach(
        "dispatch:beforeException",
        function ($event, $dispatcher) {
                    $dispatcher->forward(
                        [
                            'controller' => 'default',
                            'action'     => 'default'
                        ]
                    );
                    return false;

        }
    );
    $dispatcher = new Dispatcher();
    $dispatcher->setEventsManager($evManager);
    return $dispatcher;
}, true);


/*******************************************************************************************
 * Phalcon configuration
 */

$router->handle($_SERVER['REQUEST_URI']);
