<?php

/*
 * Modified: prepend directory path of current file, because of this file own different ENV under between Apache and command line.
 * NOTE: please remove this comment.
 */
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

//Sault for the password when it is saved on database.
const APP_SAULT = 'p2dLD6JekODROUzOCjgMlbtHLE7tLcZUxsqVyoIdIWJczHzkuIpmmB2wrsEZ';

//TODO: in staging and production to use another in order to increase the security
//Configuration of database
return new \Phalcon\Config([

    'database' => [
        'adapter'     => 'Mysql',
        'host'        => '192.168.185.2',
        'username'    => 'phalcon',
        'password'    => '1',
        'dbname'      => 'phalconDB',
        'charset'     => 'utf8',
    ],
    'application' => [
        'appDir'         => APP_PATH . '/',
        'controllersDir' => APP_PATH . '/controllers/',
        'modelsDir'      => APP_PATH . '/models/',
        'migrationsDir'  => APP_PATH . '/migrations/',
        'viewsDir'       => APP_PATH . '/views/',
        'pluginsDir'     => APP_PATH . '/plugins/',
        'libraryDir'     => APP_PATH . '/library/',
        'cacheDir'       => BASE_PATH . '/cache/',
        'baseUri'        => '/',
    ]
]);
