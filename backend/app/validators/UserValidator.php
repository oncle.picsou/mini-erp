<?php

/**
 * Class UserValidator
 *
 * The class is the validator for the Users table
 */
class UserValidator
{
    public function __construct()
    {

    }

    /**
     * The function will valid the body of the POST that the client sends in order to create the row on the
     * Users table.
     *
     * The function returns as soon as it sees an error, in order to mitigate the DDOS attack or others.
     *
     * @param object $body it is the body that the front has sent, it contains all the element like the id.
     * (it is better to split it)
     * @return bool return true if the packet is right otherwise false.
     */
    public function loginOrRegistration(object $body): bool
    {

        if (count((array)$body) != 2) {
            return false;
        }

        if (!property_exists($body, 'password')) {
            return false;
        }
        if (!property_exists($body, 'username')) {
            return false;
        }

        if (isset($body->password) === false) {
            return false;
        }
        if (isset($body->username) === false) {
            return false;
        }

        if (strlen($body->password) < 5) {
            return false;
        }

        if (strlen($body->password) > 20) {
            return false;
        }

        if (strlen($body->username) < 5) {
            return false;
        }

        if (strlen($body->username) > 20) {
            return false;
        }

        return true;
    }
}