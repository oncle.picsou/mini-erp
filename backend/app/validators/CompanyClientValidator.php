<?php

/**
 * Class CompanyClientValidator
 *
 * The class is the validator for the CompaniesClientsTransactions table
 *
 * TODO: change the name in order to improve the consistency in CompanyClientTransactionValidator
 */
class CompanyClientValidator
{
    public function __construct()
    {

    }

    /**
     * The function will valid the body of the POST/PUT that the client sends in order to create or edit the row on the
     * CompaniesClientsTransactions table.
     *
     * The function returns as soon as it sees an error, in order to mitigate the DDOS attack or others.
     *
     * @param object $body it is the body that the front has sent, it contains all the element like the id.
     * (it is better to split it)
     * @return bool return true if the packet is right otherwise false.
     */
    public function validationBody(object $body): bool
    {

        if (count((array)$body) != 5) {
            return false;
        }

        if (!property_exists($body, 'clientId')) {
            return false;
        }
        if (!property_exists($body, 'companyId')) {
            return false;
        }
        if (!property_exists($body, 'id')) {
            return false;
        }
        if (!property_exists($body, 'numbers')) {
            return false;
        }

        if ($body->clientId === null || strlen($body->clientId) < 1 || strlen($body->clientId) > 36) {
            return false;
        }
        if ($body->companyId === null || strlen($body->companyId) < 1 || strlen($body->companyId) > 36) {
            return false;
        }
        if ($body->id === null || strlen($body->id) < 1 || strlen($body->id) > 36) {
            return false;
        }

        for ($i = 0; $i < count($body->numbers); $i++) {
            if ($body->numbers[$i] === null) {
                return false;
            }
            if (!property_exists($body->numbers[$i], 'id')) {
                return false;
            }
            if (!property_exists($body->numbers[$i], 'number')) {
                return false;
            }

            if ($body->numbers[$i]->id === null || strlen($body->numbers[$i]->id) < 1 || strlen($body->numbers[$i]->id) > 36) {
                return false;
            }

            if (!is_int($body->numbers[$i]->number)) {
                return false;
            }

            if ($body->numbers[$i]->number === null ||
                strlen($body->numbers[$i]->number) < 0 ||
                strlen($body->numbers[$i]->number) > 100000) {
                return false;
            }
        }
        return true;
    }
}