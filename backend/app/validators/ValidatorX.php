<?php

/**
 * Class ValidatorX
 *
 * The class contains all the commons validators element like for the id.
 */
class ValidatorX
{
    public function __construct()
    {

    }

    /**
     * The function is able to validate the id
     *
     * @param string $id the id to validate.
     * @return bool returns true if the id is right otherwise false.
     */
    public function validationId(string $id): bool
    {
        if (strlen($id) > 36 || strlen($id) < 1) {
            return false;
        }
        return true;
    }

    /**
     * The function validate a date like this: 2021-10-27
     *
     * @param string $date the date to validate.
     * @return bool returns true if the date is right otherwise false.
     */
    public function validationDate(string $date): bool
    {
        $tmp = explode('-', $date);
        if (count($tmp) !== 3) {
            return false;
        }
        if (!checkdate($tmp[1], $tmp[2], $tmp[0])) {
            return false;
        }
        return true;
    }
}