<?php

include __DIR__ . "/ValidatorX.php";

/**
 * Class EmployeeValidator
 *
 * The class is the validator for the Employees table
 */
class EmployeeValidator
{
    private ValidatorX $validatorX;

    public function __construct()
    {
        $this->validatorX = new ValidatorX();
    }

    /**
     * The function will valid the body of the POST/PUT that the client sends in order to create or edit the row on the
     * Employees table.
     *
     * The function returns as soon as it sees an error, in order to mitigate the DDOS attack or others.
     *
     * @param object $body it is the body that the front has sent, it contains all the element like the id.
     * @param bool $isPut in order to indicate to the function if the validation is for the post or for the put
     * (it is better to split it)
     * @return bool return true if the packet is right otherwise false.
     */
    public function validationBody(object $body, bool $isPut): bool
    {
        if ($isPut) {
            $numberOfElement = 6;
            if (!property_exists($body, 'backupDbId')) {
                return false;
            }
            if ($body->backupDbId === null || strlen($body->backupDbId) < 1 || strlen($body->backupDbId) > 36) {
                return false;
            }
        } else {
            $numberOfElement = 5;
        }

        if (count((array)$body) != $numberOfElement) {
            return false;
        }
        if (!property_exists($body, 'id')) {
            return false;
        }
        if (!property_exists($body, 'birthday')) {
            return false;
        }
        if (!property_exists($body, 'country')) {
            return false;
        }
        if (!property_exists($body, 'firstDayInTheCompany')) {
            return false;
        }
        if (!property_exists($body, 'name')) {
            return false;
        }

        if ($body->id === null || strlen($body->id) < 1 || strlen($body->id) > 36) {
            return false;
        }

        if (!$body->birthday === null || strlen($body->birthday) !== 10) {
            return false;
        }
        if (!$this->validatorX->validationDate($body->birthday)) {
            return false;
        }

        if ($body->country === null || strlen($body->country) < 1 || strlen($body->country) > 100) {
            return false;
        }

        if (!$body->firstDayInTheCompany === null || strlen($body->firstDayInTheCompany) !== 10) {
            return false;
        }
        if (!$this->validatorX->validationDate($body->firstDayInTheCompany)) {
            return false;
        }

        if ($body->name === null || strlen($body->name) < 1 || strlen($body->name) > 36) {
            return false;
        }


        return true;
    }

}