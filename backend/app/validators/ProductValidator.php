<?php

/**
 * Class ProductValidator
 *
 * The class is the validator for the Products table
 */
class ProductValidator
{
    public function __construct()
    {

    }

    /**
     * The function will valid the body of the POST/PUT that the client sends in order to create or edit the row on the
     * Products table.
     *
     * The function returns as soon as it sees an error, in order to mitigate the DDOS attack or others.
     *
     * @param object $body it is the body that the front has sent, it contains all the element like the id.
     * @param bool $isPut in order to indicate to the function if the validation is for the post or for the put
     * (it is better to split it)
     * @return bool return true if the packet is right otherwise false.
     */
    public function validationBody(object $body, bool $isPut): bool
    {
        if ($isPut) {
            $numberOfElement = 6;
            if (!property_exists($body, 'backupDbId')) {
                return false;
            }
            if ($body->backupDbId === null || strlen($body->backupDbId) < 1 || strlen($body->backupDbId) > 36) {
                return false;
            }
        } else {
            $numberOfElement = 5;
        }

        if (count((array)$body) != $numberOfElement) {
            return false;
        }
        if (!property_exists($body, 'id')) {
            return false;
        }
        if (!property_exists($body, 'name')) {
            return false;
        }
        if (!property_exists($body, 'price')) {
            return false;
        }
        if (!property_exists($body, 'tax')) {
            return false;
        }
        if (!property_exists($body, 'stock')) {
            return false;
        }

        if ($body->id === null || strlen($body->id) < 1 || strlen($body->id) > 36) {
            return false;
        }

        if ($body->name === null || strlen($body->name) < 1 || strlen($body->name) > 100) {
            return false;
        }

        if (!is_float($body->price) && !is_int($body->price)) {
            return false;
        }
        if ($body->price < 0 || $body->price > 100000) {
            return false;
        }


        if (!is_float($body->tax) && !is_int($body->tax)) {
            return false;
        }
        if ($body->tax < 0 || $body->tax > 100000) {
            return false;
        }

        if (!is_int($body->stock)) {
            return false;
        }
        if ($body->stock < 0 || $body->stock > 100000) {
            return false;
        }
        return true;
    }
}