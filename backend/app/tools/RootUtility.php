<?php

use Phalcon\Session\ManagerInterface;

include __DIR__ . "/../tools/PhalconSession.php";

/**
 * Class RootUtility
 *
 * The RootUtility class is the entry point for all the commons actions that the controllers have to do like
 * for the session. The idea is: the controller calls RootUtility class and RootUtility constructor will create
 * all the infrastructure (ex: session, redis, database, logs ecc...).
 *
 * In other words RootUtility class is wrapper for the common components.
 */
class RootUtility
{
    private static RootUtility $_instance;
    public ManagerInterface $session;
    public PhalconSession $phalconSession;

    /**
     * The constructor is able to create all the infrastructure (other class) that the developer could call in others
     * points of the backend.
     *
     * @param ManagerInterface $session the session of the framework.
     */
    public function __construct(ManagerInterface $session)
    {
        self::$_instance=$this;
        $this->session = $session;
        $this->phalconSession = new PhalconSession($this);
    }

    /**
     * The function is an experimental another way to create the class by singleton and accelerate all the process and
     * to have other item, but it is to investigate. For the moment it is on backlog.
     *
     * @return RootUtility|null must return only RootUtility object because the class must be a singleton.
     */
    public static function getInstance():?RootUtility
    {
        if(is_null(self::$_instance))
        {
            // TODO: To create the RootUtility and not to return null, but there are the parameters to pass constructor.
            return null;
        }
        return self::$_instance;
    }


}