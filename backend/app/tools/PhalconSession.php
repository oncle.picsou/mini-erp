<?php

use Phalcon\Session\ManagerInterface;

include __DIR__ . "/../constants/Constants.php";

/**
 * Class PhalconSession
 * The class is a wrapper for the real session in order to avoid putting on the code try/catch and other stuffs.
 *
 * The idea is simple: if one day Phalcon changes the way to manage the session, the developer has to touch only this
 * class.
 *
 * WARNING:
 * Every function is able to see if the session has already started or no.
 * If the session has not already started, the function will call the right function in order to start the session.
 */
class PhalconSession
{
    public ManagerInterface $session;
    private bool $isStartedSession = false;

    /**
     * The constructor get the RootUtility that contains the real session.
     * The idea is: the entry point (the controller) calls the RootUtility passing all the parameters and
     * RootUtility create all the others.
     *
     * @param RootUtility $rootUtility
     */
    public function __construct(RootUtility $rootUtility)
    {
        $this->session = $rootUtility->session;
    }

    /**
     * To set the user as logged in.
     *
     * @return bool returns true if the function has set the user logged otherwise false.
     */
    public function logged(): bool
    {
        if(!$this->isStartedSession) {
            if(!$this->startSession()){
                return false;
            }
            $this->isStartedSession = true;
        }

        try {
            $this->session->set(Constants::isLogged, true);
        } catch (Exception $ex) {
            return false;
        }
        return true;
    }

    /**
     * To set the user as logout.
     *
     * @return bool returns true if the function has set the user as logout otherwise false.
     */
    public function logout(): bool
    {
        if(!$this->isStartedSession) {
            if(!$this->startSession()){
                return false;
            }
            $this->isStartedSession = true;
        }

        try {
            $this->session->destroy();
        } catch (Exception $ex) {
            return false;
        }
        return true;
    }

    /**
     * The function is able to check if the user has logged or no.
     *
     * @param bool $isLogged the function sets it on true if the user is logged otherwise false.
     * @return bool returns true if the function has not had problems otherwise false.
     */
    public function isLogged(bool &$isLogged): bool
    {
        if(!$this->isStartedSession) {
            if(!$this->startSession()) {
                return false;
            }
            $this->isStartedSession = true;
        }

        try {
            $isLogged = $this->session->get(Constants::isLogged);
        } catch (Exception $ex) {
            return false;
        }
        return true;
    }

    /**
     * The function is able to start the function
     *
     * @return bool returns true if the function has well started otherwise false.
     */
    public function startSession(): bool
    {
        try {
            $this->session->start();
        } catch (Exception $ex) {
            return false;
        }
        return true;

    }

}