<?php

class CompaniesClientsTransactions extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $id;

    /**
     *
     * @var string
     */
    public $clientId;

    /**
     *
     * @var string
     */
    public $companyId;

    /**
     *
     * @var string
     */
    public $employeeId;

    /**
     *
     * @var string
     */
    public $time;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("phalconDB");
        $this->setSource("CompaniesClientsTransactions");
        $this->belongsTo('clientId', '\Clients', 'id', ['alias' => 'Clients']);
        $this->belongsTo('companyId', '\Companies', 'id', ['alias' => 'Companies']);
        $this->belongsTo('employeeId', '\Employees', 'id', ['alias' => 'Employees']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CompaniesClientsTransactions[]|CompaniesClientsTransactions|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CompaniesClientsTransactions|\Phalcon\Mvc\Model\ResultInterface|\Phalcon\Mvc\ModelInterface|null
     */
    public static function findFirst($parameters = null): ?\Phalcon\Mvc\ModelInterface
    {
        return parent::findFirst($parameters);
    }

}
