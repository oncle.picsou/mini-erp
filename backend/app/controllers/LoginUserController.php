<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

include __DIR__ . "/../tools/RootUtility.php";
include __DIR__ . "/../validators/UserValidator.php";

/**
 * Class LoginUserController
 *
 * @property Dispatcher $dispatcher
 */
class LoginUserController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function loginUserAction(): ResponseInterface
    {
        $this->view->disable();

        $body = $this->request->getJsonRawBody();
        if (!(new UserValidator())->loginOrRegistration($body)) {
            return $this->response->setStatusCode(400);
        }

        $user = Users::find("username = '$body->username'");

        if (count($user) > 1 || count($user) === 0) {
            return $this->response->setStatusCode(500);
        }

        if (strcmp($user[0]->username, $body->username) != 0) {
            return $this->response->setStatusCode(500);
        }

        if (!$this->security->checkHash($body->password . $user[0]->sault . APP_SAULT, $user[0]->password)) {
            return $this->response->setStatusCode(500);
        }

        $rootUtility = new RootUtility($this->session);
        if (!$rootUtility->phalconSession->startSession()) {
            return $this->response->setStatusCode(500);
        }

        if (!$rootUtility->phalconSession->logged()) {
            return $this->response->setStatusCode(500);
        }
        return $this->response->setContent(200);
    }
}