<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

/**
 * Class GetClientController
 *
 * @property Dispatcher $dispatcher
 */
class GetClientController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function getClientAction(): ResponseInterface
    {
        $this->view->disable();

        try {
            $users = $this->modelsManager->executeQuery('SELECT * FROM Clients', []);
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }

        return $this->response->setStatusCode(200)->setContent(json_encode($users));
    }
}