<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

include __DIR__ . "/../tools/RootUtility.php";
include __DIR__ . "/../validators/ClientValidator.php";

/**
 * Class PostClientController
 *
 * @property Dispatcher $dispatcher
 */
class PostClientController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function postClientAction(): ResponseInterface
    {
        $this->view->disable();

        $isLogged = true;
        if (!(new RootUtility($this->session))->phalconSession->isLogged($isLogged)) {
            return $this->response->setStatusCode(500);
        }

        if (!$isLogged) {
            return $this->response->setStatusCode(403);
        }

        $body = $this->request->getJsonRawBody();
        if (!(new ClientValidator())->validationBody($body, false)) {
            return $this->response->setStatusCode(400);
        }

        try {
            $query = 'INSERT INTO Clients (id, name, address, country) VALUES(:id:, :name:, :address:, :country:)';
            $result = $this->modelsManager->executeQuery($query,
                ['id' => $body->id, 'name' => $body->name, 'address' => $body->address, 'country' => $body->country]);
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }

        if (!$result->success()) {
            return $this->response->setStatusCode(500);
        }

        return $this->response->setStatusCode(201);
    }
}