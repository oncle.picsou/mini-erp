<?php

use Phalcon\Db\Column;
use Phalcon\Http\ResponseInterface;
use Phalcon\Http\Response;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

include __DIR__ . "/../tools/RootUtility.php";
include __DIR__ . "/../validators/ProviderValidator.php";

/**
 * Class PutProviderController
 *
 * @property Dispatcher $dispatcher
 */
class PutProviderController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function putProviderAction(): ResponseInterface
    {
        $this->view->disable();

        $isLogged = true;
        if (!(new RootUtility($this->session))->phalconSession->isLogged($isLogged)) {
            return $this->response->setStatusCode(500);
        }

        if (!$isLogged) {
            return $this->response->setStatusCode(403);
        }

        $body = $this->request->getJsonRawBody();
        if (!(new ProviderValidator())->validationBody($body, true)) {
            $response = new Response();
            $response->setStatusCode(400);
        }

        try {
            $result = $this->db->execute(
                'UPDATE Providers SET id = ?, name = ?, address = ?, country = ? WHERE id =  ?',
                [
                    $body->id,
                    $body->name,
                    $body->address,
                    $body->country,
                    $body->backupDbId
                ],
                [
                    Column::BIND_PARAM_STR,
                    Column::BIND_PARAM_STR,
                    Column::BIND_PARAM_STR,
                    Column::BIND_PARAM_STR,
                    Column::BIND_PARAM_STR
                ]);
            if (!$result) {
                return $this->response->setStatusCode(500);
            }
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }
        return $this->response->setStatusCode(200);
    }
}