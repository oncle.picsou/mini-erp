<?php


use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

include __DIR__ . "/../tools/RootUtility.php";
include __DIR__ . "/../validators/EmployeeValidator.php";

/**
 * Class PostEmployeeController
 *
 * @property Dispatcher $dispatcher
 */
class PostEmployeeController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function postEmployeeAction(): ResponseInterface
    {
        $this->view->disable();

        $isLogged = true;
        if (!(new RootUtility($this->session))->phalconSession->isLogged($isLogged)) {
            return $this->response->setStatusCode(500);
        }

        if (!$isLogged) {
            return $this->response->setStatusCode(403);
        }

        $body = $this->request->getJsonRawBody();

        if (!(new EmployeeValidator())->validationBody($body, false)) {
            return $this->response->setStatusCode(400);
        }

        try {
            $query = 'INSERT INTO Employees (id, name, birthday, country, firstDayInTheCompany) VALUES(
                                                                        :id:, :name:, :birthday:, :country:, :firstDayInTheCompany:)';
            $result = $this->modelsManager->executeQuery($query,
                [
                    'id' => $body->id,
                    'name' => $body->name,
                    'birthday' => $body->birthday,
                    'country' => $body->country,
                    'firstDayInTheCompany' => $body->firstDayInTheCompany
                ]);
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }

        if (!$result->success()) {
            return $this->response->setStatusCode(500);
        }

        return $this->response->setStatusCode(201);
    }
}