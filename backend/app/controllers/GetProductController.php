<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

/**
 * Class DeleteEmployeeController
 *
 * @property Dispatcher $dispatcher
 */
class GetProductController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function getProductAction(): ResponseInterface
    {
        $this->view->disable();

        try {
            $products = $this->modelsManager->executeQuery('SELECT * FROM Products', []);
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }

        return $this->response->setStatusCode(200)->setContent(json_encode($products));
    }
}