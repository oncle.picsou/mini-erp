<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

include __DIR__ . "/../tools/RootUtility.php";
include __DIR__ . "/../validators/ProviderValidator.php";

/**
 * Class PostProviderController
 *
 * @property Dispatcher $dispatcher
 */
class PostProviderController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function postProviderAction(): ResponseInterface
    {
        $this->view->disable();

        $isLogged = false;
        if (!(new RootUtility($this->session))->phalconSession->isLogged($isLogged)) {
            return $this->response->setStatusCode(500);
        }

        if (!$isLogged) {
            return $this->response->setStatusCode(403);
        }

        $body = $this->request->getJsonRawBody();
        if (!(new ProviderValidator())->validationBody($body, false)) {
            return $this->response->setStatusCode(400);
        }

        try {
            $query = 'INSERT INTO Providers (id, name, address, country) VALUES(:id:, :name:, :address:, :country:)';
            $result = $this->modelsManager->executeQuery($query,
                [
                    'id' => $body->id,
                    'name' => $body->name,
                    'address' => $body->address,
                    'country' => $body->country
                ]);
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }

        if (!$result->success()) {
            return $this->response->setStatusCode(500);
        }

        return $this->response->setStatusCode(201);
    }
}