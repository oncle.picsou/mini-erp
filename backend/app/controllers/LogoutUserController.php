<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

include __DIR__ . "/../tools/RootUtility.php";

/**
 * Class LogoutUserController
 *
 * @property Dispatcher $dispatcher
 */
class LogoutUserController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function logoutUserAction(): ResponseInterface
    {
        if (!(new RootUtility($this->session))->phalconSession->logout()) {
            return $this->response->setStatusCode(500);
        }
        return $this->response->setStatusCode(200);
    }
}