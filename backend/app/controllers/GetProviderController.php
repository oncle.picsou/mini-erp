<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

/**
 * Class GetProviderController
 *
 * @property Dispatcher $dispatcher
 */
class GetProviderController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function getProviderAction(): ResponseInterface
    {
        $this->view->disable();

        try {
            $providers = $this->modelsManager->executeQuery('SELECT * FROM Providers', []);
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }

        return $this->response->setStatusCode(200)->setContent(json_encode($providers));
    }
}