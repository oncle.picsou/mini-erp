<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

/**
 * Class GetCompanyClientController
 *
 * @property Dispatcher $dispatcher
 */
class GetCompanyClientController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function getCompanyClientAction(): ResponseInterface
    {
        $this->view->disable();

        try {
            $companiesClientsTransactions =
                $this->modelsManager->executeQuery('SELECT * FROM CompaniesClientsTransactions', []);
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }

        return $this->response->setStatusCode(200)->setContent(json_encode($companiesClientsTransactions));
    }
}