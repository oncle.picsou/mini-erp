<?php


use Phalcon\Db\Column;
use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

include __DIR__ . "/../tools/RootUtility.php";
include __DIR__ . "/../validators/CompanyClientValidator.php";

/**
 * Class PostCompanyClientController
 *
 * @property Dispatcher $dispatcher
 */
class PostCompanyClientController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function postCompanyClientAction(): ResponseInterface
    {
        $this->view->disable();

        $isLogged = true;
        if (!(new RootUtility($this->session))->phalconSession->isLogged($isLogged)) {
            return $this->response->setStatusCode(500);
        }

        if (!$isLogged) {
            return $this->response->setStatusCode(403);
        }

        $body = $this->request->getJsonRawBody();
        if (!(new CompanyClientValidator())->validationBody($body)) {
            return $this->response->setStatusCode(400);
        }


        $productIds = [];
        $types = [];
        for ($i = 0; $i < count($body->numbers); $i++) {
            array_push($productIds, $body->numbers[$i]->id);
            array_push($productIds, $body->numbers[$i]->number);
            array_push($types, Column::BIND_PARAM_STR);
            array_push($types, Column::BIND_PARAM_INT);
        }


        //Transaction
        //https://docs.phalcon.io/4.0/en/db-models-transactions
        try {
            //Start transaction be careful to finish it with $this->db->commit();
            $this->db->begin();

            //Check if there is enough stock for this transaction
            $result = $this->db->query('SELECT * FROM Products WHERE ' .
                $this->createStringIds(count($body->numbers)), $productIds, $types);

            //Not enough stock
            if ($result->numRows() == 0) {
                $this->db->rollback();
                return $this->response->setStatusCode(500);
            }

            //Update stock
            $arrayResult = $result->fetchAll();
            for ($x = 0; $x < count($arrayResult); $x++) {
                for ($y = 0; $y < count($body->numbers); $y++) {
                    if ($body->numbers[$y]->id === $arrayResult[$x]['id']) {
                        if (!$this->db->execute(
                            'UPDATE Products SET stock = ? WHERE id =  ?',
                            [
                                $arrayResult[$x]['stock'] - $body->numbers[$y]->number,
                                $body->numbers[$y]->id
                            ],
                            [
                                Column::BIND_PARAM_INT,
                                Column::BIND_PARAM_STR
                            ]
                        )) {
                            $this->db->rollback();
                            return $this->response->setStatusCode(500);
                        }
                    }
                }
            }


            //[Question] is it to calculate company's earn?

            //Insert new transaction
            $result = $this->db->execute('INSERT INTO CompaniesClientsTransactions (
                                          id, clientId, companyId, employeeId, time) VALUES(
                                                                        ?, ?, ?, ?, ?)',
                [
                    $body->id,
                    $body->clientId,
                    $body->companyId,
                    $body->employeeId,
                    gmdate('Y-m-d h:i:s') //add with GMT time zone

                ],
                [
                    Column::BIND_PARAM_STR,
                    Column::BIND_PARAM_STR,
                    Column::BIND_PARAM_STR,
                    Column::TYPE_TIMESTAMP
                ]);

            if ($result === false) {
                $this->db->rollback();
                return $this->response->setStatusCode(500);

            }

            //The commit it is mandatory if $this->db->begin(); has been used
            $this->db->commit();
        } catch (Exception $e) {
            $this->db->rollback();
            return $this->response->setStatusCode(500);
        }


        return $this->response->setStatusCode(201);
    }

    private function createStringIds(int $numberId): string
    {
        $stringIds = '(id = ? and stock >= ?)';
        $id = ' || (id = ? and stock >= ?)';
        for ($i = 1; $i < $numberId; $i++) {
            $stringIds = $stringIds . $id;
        }
        return $stringIds;
    }
}