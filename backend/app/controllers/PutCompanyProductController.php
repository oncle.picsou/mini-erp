<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

include __DIR__ . "/../tools/RootUtility.php";

/**
 * Class PutCompanyProductController
 *
 * @property Dispatcher $dispatcher
 */
class PutCompanyProductController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function putCompanyProductAction(): ResponseInterface
    {
        $this->view->disable();

        $isLogged = true;
        if (!(new RootUtility($this->session))->phalconSession->isLogged($isLogged)) {
            return $this->response->setStatusCode(500);
        }

        if (!$isLogged) {
            return $this->response->setStatusCode(403);
        }

        return $this->response->setStatusCode(501);
    }
}