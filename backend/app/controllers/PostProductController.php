<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

include __DIR__ . "/../tools/RootUtility.php";
include __DIR__ . "/../validators/ProductValidator.php";

/**
 * Class PostProductController
 *
 * @property Dispatcher $dispatcher
 */
class PostProductController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function postProductAction(): ResponseInterface
    {
        $this->view->disable();

        $isLogged = false;
        if (!(new RootUtility($this->session))->phalconSession->isLogged($isLogged)) {
            return $this->response->setStatusCode(500);
        }

        if (!$isLogged) {
            return $this->response->setStatusCode(403);
        }

        $body = $this->request->getJsonRawBody();
        if (!(new ProductValidator())->validationBody($body, false)) {
            return $this->response->setStatusCode(400);
        }

        try {
            $query = 'INSERT INTO Products (id, name, price, tax, stock) VALUES(
                                                          :id:, :name:, :price:, :tax:, :stock:)';
            $result = $this->modelsManager->executeQuery($query,
                [
                    'id' => $body->id,
                    'name' => $body->name,
                    'price' => $body->price,
                    'tax' => $body->tax,
                    'stock' => $body->stock,
                    'idCompany' => $body->idCompany
                ]);
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }

        if (!$result->success()) {
            return $this->response->setStatusCode(500);
        }

        return $this->response->setStatusCode(201);
    }
}