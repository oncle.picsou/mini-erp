<?php


use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Security;
use Phalcon\Security\Random;

include __DIR__ . "/../validators/UserValidator.php";

/**
 * Class RegistrationUserController
 *
 * @property Dispatcher $dispatcher
 */
class RegistrationUserController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function registrationUserAction(): ResponseInterface
    {
        $this->view->disable();


        $body = $this->request->getJsonRawBody();
        if (!(new UserValidator())->loginOrRegistration($body)) {
            return $this->response->setStatusCode(400);
        }

        $userCheck = Users::find("username = '$body->username'");

        if (count($userCheck) != 0) {
            return $this->response->setStatusCode(409);
        }

        $user = new Users();
        $security = new Security();
        $random = new Random();

        try {
            $user->id = $random->uuid();
            $user->username = $body->username;
            $user->sault = $security->hash($random->uuid());
            $user->password = $security->hash($body->password . $user->sault . APP_SAULT);


            if (!$user->save()) {
                return $this->response->setStatusCode(500);
            }
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }
        return $this->response->setStatusCode(201);
    }
}