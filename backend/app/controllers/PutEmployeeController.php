<?php

use Phalcon\Db\Column;
use Phalcon\Http\ResponseInterface;
use Phalcon\Http\Response;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

include __DIR__ . "/../tools/RootUtility.php";
include __DIR__ . "/../validators/EmployeeValidator.php";

/**
 * Class PutEmployeeController
 *
 * @property Dispatcher $dispatcher
 */
class PutEmployeeController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function putEmployeeAction(): ResponseInterface
    {
        $this->view->disable();

        $isLogged = true;
        if (!(new RootUtility($this->session))->phalconSession->isLogged($isLogged)) {
            return $this->response->setStatusCode(500);
        }

        if (!$isLogged) {
            return $this->response->setStatusCode(403);
        }

        $body = $this->request->getJsonRawBody();
        if (!(new EmployeeValidator())->validationBody($body, true)) {
            return $this->response->setStatusCode(500);
        }

        try {
            $result = $this->db->execute(
                'UPDATE Employees
                SET id = ?, name = ?, birthday = ?, country = ?, firstDayInTheCompany = ? WHERE id = ?',
                [
                    $body->id,
                    $body->name,
                    $body->birthday,
                    $body->country,
                    $body->firstDayInTheCompany,
                    $body->backupDbId
                ],
                [
                    Column::BIND_PARAM_STR,
                    Column::BIND_PARAM_STR,
                    Column::TYPE_TIMESTAMP,
                    Column::BIND_PARAM_STR,
                    Column::TYPE_TIMESTAMP,
                    Column::BIND_PARAM_STR
                ]);

            if (!$result) {
                return $this->response->setStatusCode(500);
            }
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }
        return $this->response->setStatusCode(200);
    }
}