<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

include __DIR__ . "/../tools/RootUtility.php";
include __DIR__ . "/../validators/CompanyValidator.php";

/**
 * Class PostCompanyController
 *
 * @property Dispatcher $dispatcher
 */
class PostCompanyController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function postCompanyAction(): ResponseInterface
    {
        $this->view->disable();


        $isLogged = true;
        if (!(new RootUtility($this->session))->phalconSession->isLogged($isLogged)) {
            return $this->response->setStatusCode(500);
        }

        if (!$isLogged) {
            return $this->response->setStatusCode(403);
        }

        $body = $this->request->getJsonRawBody();
        if (!(new CompanyValidator())->validationBody($body, false)) {
            return $this->response->setStatusCode(400);
        }

        try {
            $query = 'INSERT INTO Companies (id, name, balance, country) VALUES(:id:, :name:, :balance:, :country:)';
            $result = $this->modelsManager->executeQuery($query,
                [
                    'id' => $body->id,
                    'name' => $body->name,
                    'balance' => $body->balance,
                    'country' => $body->country
                ]);
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }

        if (!$result->success()) {
            return $this->response->setStatusCode(500);
        }

        return $this->response->setStatusCode(201);
    }
}