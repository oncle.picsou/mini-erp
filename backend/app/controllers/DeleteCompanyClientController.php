<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

include __DIR__ . "/../tools/RootUtility.php";
include __DIR__ . "/../validators/ValidatorX.php";


/**
 * Class DeleteCompanyClientController
 *
 * @property Dispatcher $dispatcher
 */
class DeleteCompanyClientController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function deleteCompanyClientAction(): ResponseInterface
    {
        $this->view->disable();

        $isLogged = false;
        if (!(new RootUtility($this->session))->phalconSession->isLogged($isLogged)) {
            return $this->response->setStatusCode(500);
        }

        if (!$isLogged) {
            return $this->response->setStatusCode(403);
        }

        $id = $this->dispatcher->getParam('id');

        if (!(new ValidatorX())->validationId($id)) {
            return $this->response->setStatusCode(400);
        }

        try {
            $query = 'DELETE FROM CompaniesClientsTransactions WHERE id = :id:';
            $result = $this->modelsManager->executeQuery($query, ['id' => $id]);
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }

        if (!$result->success()) {
            return $this->response->setStatusCode(500);
        }
        return $this->response->setStatusCode(200);


    }
}