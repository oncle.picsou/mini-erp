<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

include __DIR__ . "/../tools/RootUtility.php";
include __DIR__ . "/../validators/ValidatorX.php";

/**
 * Class DeleteProductController
 *
 * @property Dispatcher $dispatcher
 */
class DeleteProductController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function deleteProductAction(): ResponseInterface
    {
        $this->view->disable();

        $isLogged = false;
        if (!(new RootUtility($this->session))->phalconSession->isLogged($isLogged)) {
            return $this->response->setStatusCode(500);
        }

        if (!$isLogged) {
            return $this->response->setStatusCode(403);
        }

        $idProvider = $this->dispatcher->getParam('id');

        if (!(new ValidatorX())->validationId($idProvider)) {
            return $this->response->setStatusCode(400);
        }

        try {
            $query = 'DELETE FROM Products WHERE id = :id:';
            $result = $this->modelsManager->executeQuery($query, ['id' => $idProvider]);
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }

        if (!$result->success()) {
            return $this->response->setStatusCode(500);
        }
        return $this->response->setStatusCode(200);


    }
}