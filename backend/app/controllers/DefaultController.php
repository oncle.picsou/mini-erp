<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

/**
 * Class DefaultController
 * The controller is the default controller in order to redirect all the false requests (not right requests).
 *
 * @property Dispatcher $dispatcher
 */
class DefaultController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function defaultAction(): ResponseInterface
    {
        $this->view->disable();

        return $this->response->setStatusCode(404);

    }
}