<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

/**
 * Class GetEmployeeController
 *
 * @property Dispatcher $dispatcher
 */
class GetEmployeeController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function getEmployeeAction(): ResponseInterface
    {
        $this->view->disable();

        try {
            $employees = $this->modelsManager->executeQuery('SELECT * FROM Employees', []);
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }

        return $this->response->setStatusCode(200)->setContent(json_encode($employees));
    }
}