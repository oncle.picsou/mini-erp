<?php

use Phalcon\Db\Column;
use Phalcon\Http\ResponseInterface;
use Phalcon\Http\Response;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

include __DIR__ . "/../tools/RootUtility.php";
include __DIR__ . "/../validators/ProviderValidator.php";

/**
 * Class PutProductController
 *
 * @property Dispatcher $dispatcher
 */
class PutProductController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function putProductAction(): ResponseInterface
    {
        $this->view->disable();

        $isLogged = true;
        if (!(new RootUtility($this->session))->phalconSession->isLogged($isLogged)) {
            return $this->response->setStatusCode(500);
        }

        if (!$isLogged) {
            return $this->response->setStatusCode(403);
        }

        $body = $this->request->getJsonRawBody();
        if (!(new ProviderValidator())->validationBody($body, true)) {
            $response = new Response();
            $response->setStatusCode(400);
        }

        try {
            $result = $this->db->execute(
                'UPDATE Products SET id = ?, name = ?, price = ?, tax = ?, stock = ? WHERE id =  ?',
                [
                    $body->id,
                    $body->name,
                    $body->price,
                    $body->tax,
                    $body->stock,
                    $body->backupDbId
                ],
                [
                    Column::BIND_PARAM_STR,
                    Column::BIND_PARAM_STR,
                    Column::BIND_PARAM_STR,
                    Column::BIND_PARAM_STR,
                    Column::BIND_PARAM_STR
                ]);
            if (!$result) {
                return $this->response->setStatusCode(500);
            }
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }
        return $this->response->setStatusCode(200);
    }
}