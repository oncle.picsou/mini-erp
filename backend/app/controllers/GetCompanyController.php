<?php

use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

/**
 * Class GetCompanyController
 *
 * @property Dispatcher $dispatcher
 */
class GetCompanyController extends Controller
{
    /**
     * @return ResponseInterface
     */
    public function getCompanyAction(): ResponseInterface
    {
        $this->view->disable();

        try {
            $companies = $this->modelsManager->executeQuery('SELECT * FROM Companies', []);
        } catch (Exception $e) {
            return $this->response->setStatusCode(500);
        }

        return $this->response->setStatusCode(200)->setContent(json_encode($companies));
    }
}