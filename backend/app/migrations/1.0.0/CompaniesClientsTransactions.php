<?php

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Migrations\Mvc\Model\Migration;

/**
 * Class CompaniesclientstransactionsMigration_100
 */
class CompaniesclientstransactionsMigration_100 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('CompaniesClientsTransactions', [
                'columns' => [
                    new Column(
                        'id',
                        [
                            'type' => Column::TYPE_CHAR,
                            'notNull' => true,
                            'size' => 36,
                            'first' => true
                        ]
                    ),
                    new Column(
                        'clientId',
                        [
                            'type' => Column::TYPE_CHAR,
                            'notNull' => false,
                            'size' => 36,
                            'after' => 'id'
                        ]
                    ),
                    new Column(
                        'companyId',
                        [
                            'type' => Column::TYPE_CHAR,
                            'notNull' => false,
                            'size' => 36,
                            'after' => 'clientId'
                        ]
                    ),
                    new Column(
                        'employeeId',
                        [
                            'type' => Column::TYPE_CHAR,
                            'notNull' => false,
                            'size' => 36,
                            'after' => 'companyId'
                        ]
                    ),
                    new Column(
                        'time',
                        [
                            'type' => Column::TYPE_TIMESTAMP,
                            'default' => "current_timestamp() on update current_timestamp()",
                            'notNull' => true,
                            'after' => 'employeeId'
                        ]
                    )
                ],
                'indexes' => [
                    new Index('PRIMARY', ['id'], 'PRIMARY'),
                    new Index('clientId', ['clientId'], ''),
                    new Index('companyId', ['companyId'], ''),
                    new Index('employeeId', ['employeeId'], '')
                ],
                'references' => [
                    new Reference(
                        'CompaniesClientsTransactions_ibfk_1',
                        [
                            'referencedSchema' => 'phalconDB',
                            'referencedTable' => 'Clients',
                            'columns' => ['clientId'],
                            'referencedColumns' => ['id'],
                            'onUpdate' => 'RESTRICT',
                            'onDelete' => 'RESTRICT'
                        ]
                    ),
                    new Reference(
                        'CompaniesClientsTransactions_ibfk_2',
                        [
                            'referencedSchema' => 'phalconDB',
                            'referencedTable' => 'Companies',
                            'columns' => ['companyId'],
                            'referencedColumns' => ['id'],
                            'onUpdate' => 'RESTRICT',
                            'onDelete' => 'RESTRICT'
                        ]
                    ),
                    new Reference(
                        'CompaniesClientsTransactions_ibfk_3',
                        [
                            'referencedSchema' => 'phalconDB',
                            'referencedTable' => 'Employees',
                            'columns' => ['employeeId'],
                            'referencedColumns' => ['id'],
                            'onUpdate' => 'RESTRICT',
                            'onDelete' => 'RESTRICT'
                        ]
                    )
                ],
                'options' => [
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8mb4_general_ci'
                ],
            ]
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
