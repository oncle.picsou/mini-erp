## Single table
CREATE TABLE Clients
(
    id      char(36)  NOT NULL,
    name    char(100) NOT NULL,
    address char(255) NOT NULL,
    country char(100) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name)
);

CREATE TABLE Companies
(
    id      char(36)  NOT NULL,
    name    char(100) NOT NULL,
    balance float     NOT NULL DEFAULT 0,
    country char(100) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name)
);

CREATE TABLE Employees
(
    id                   char(36)  NOT NULL,
    name                 char(100) NOT NULL,
    birthday             date      NOT NULL,
    country              char(100) NOT NULL,
    firstDayInTheCompany date      NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name)
);


CREATE TABLE Products
(
    id    char(36)  NOT NULL,
    name  char(100) NOT NULL,
    price float     NOT NULL,
    tax   float     NOT NULL,
    stock int(11)   NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name)
);

CREATE TABLE Providers
(
    id      char(36)  NOT NULL,
    name    char(100) NOT NULL,
    address char(255) NOT NULL,
    country char(100) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name)
);

CREATE TABLE Users
(
    id       char(36) NOT NULL,
    username char(20) NOT NULL,
    password char(60) NOT NULL,
    sault    char(60) NOT NULL
);

## Relation tables
CREATE TABLE CompaniesClientsTransactions
(
    id         char(36),
    clientId   char(36),
    companyId  char(36),
    employeeId char(36),
    time       timestamp,
    PRIMARY KEY (id),
    FOREIGN KEY (clientId) REFERENCES Clients (id),
    FOREIGN KEY (companyId) REFERENCES Companies (id),
    FOREIGN KEY (employeeId) REFERENCES Employees (id)
);
