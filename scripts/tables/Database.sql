## Single table
CREATE TABLE Clients
(
    id      char(36)  NOT NULL,
    name    char(100) NOT NULL,
    address char(255) NOT NULL,
    country char(100) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name)
);

CREATE TABLE Companies
(
    id      char(36)  NOT NULL,
    name    char(100) NOT NULL,
    balance float     NOT NULL DEFAULT 0,
    country char(100) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name)
);


CREATE TABLE Employees
(
    id                   char(36)  NOT NULL,
    name                 char(100) NOT NULL,
    birthday             date      NOT NULL,
    country              char(100) NOT NULL,
    firstDayInTheCompany date      NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name)
);

CREATE TABLE EmployeeWorksForCompanies
(
    id         char(36) NOT NULL,
    companyId  char(36) NOT NULL,
    employeeId char(36) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (companyId) REFERENCES Companies (id),
    FOREIGN KEY (employeeId) REFERENCES Employees (id)
);

CREATE TABLE Products
(
    id        char(36)  NOT NULL,
    name      char(100) NOT NULL,
    price     float     NOT NULL,
    tax       float     NOT NULL,
    stock     int(11)   NOT NULL,
    companyId char(100) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name),
    FOREIGN KEY (companyId) REFERENCES Companies (id)
);

CREATE TABLE Providers
(
    id      char(36)  NOT NULL,
    name    char(100) NOT NULL,
    address char(255) NOT NULL,
    country char(100) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY (name)
);

CREATE TABLE Users
(
    id       char(36) NOT NULL,
    username char(20) NOT NULL,
    password char(60) NOT NULL,
    sault    char(60) NOT NULL,
    isAdmin  bool
);

## Transaction tables
CREATE TABLE CompaniesClientsTransactions
(
    id         char(36),
    clientId   char(36),
    companyId  char(36),
    employeeId char(36),
    time       timestamp,
    PRIMARY KEY (id),
    FOREIGN KEY (clientId) REFERENCES Clients (id),
    FOREIGN KEY (companyId) REFERENCES Companies (id),
    FOREIGN KEY (employeeId) REFERENCES Employees (id)
);


CREATE TABLE ProvidersCompaniesTransactions
(
    id         char(36),
    companyId  char(36),
    employeeId char(36),
    providerId char(36),
    time       timestamp,
    PRIMARY KEY (id),
    FOREIGN KEY (companyId) REFERENCES Companies (id),
    FOREIGN KEY (employeeId) REFERENCES Employees (id),
    FOREIGN KEY (providerId) REFERENCES Providers (id)
);


## History of the transaction
CREATE TABLE ProductsQuantitiesCompaniesClientsTransactions
(
    id                            char(36),
    idProduct                     char(36),
    quantityOfProduct             char(36),
    cost                          int,
    companiesClientsTransactionId char(36),
    PRIMARY KEY (id),
    FOREIGN KEY (idProduct) REFERENCES Products (id),
    FOREIGN KEY (companiesClientsTransactionId) REFERENCES CompaniesClientsTransactions (id)
);

CREATE TABLE ProductsQuantitiesProvidersCompaniesTransactions
(
    id                              char(36),
    idProduct                       char(36),
    quantityOfProduct               char(36),
    cost                            int,
    providersCompaniesTransactionId char(36),
    PRIMARY KEY (id),
    FOREIGN KEY (idProduct) REFERENCES Products (id),
    FOREIGN KEY (providersCompaniesTransactionId) REFERENCES ProvidersCompaniesTransactions (id)
);