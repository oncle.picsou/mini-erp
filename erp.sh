# These two lines of code are necessary to get the alias from .bashrc files.
#Sometimes are important if the tools are not installed on the system but only like alias.
shopt -s expand_aliases

# source ~/.bashrc for old macOs or Unix(ex: Kali)
if [[ "$OSTYPE" == "darwin"* ]]; then
  # Mac OSX
  source ~/.zshrc
else
  # Maybe not necessary on other platforms
  echo "the showing feature is not implemented on this platform"
fi

############################################################################
################################## Constants ###############################

## Paths
#userX="/Users/fxr/phalcon-devtools/phalcon server"
backendFolder=backend
frontendFolder=frontend
mochaFrontendTestFolder=mocha-frontend-test

## Color for the terminal
#Black        0;30     Dark Gray     1;30
#Red          0;31     Light Red     1;31
#Green        0;32     Light Green   1;32
#Brown/Orange 0;33     Yellow        1;33
#Blue         0;34     Light Blue    1;34
#Purple       0;35     Light Purple  1;35
#Cyan         0;36     Light Cyan    1;36
#Light Gray   0;37     White         1;37
red='\033[0;31m'
reset='\033[0m'

############################################################################
################################## PROXY ###################################

########################################################################################################################

############################################################################
################################## MAIN ####################################
run_main() {
  typeNumber="moon"
  while [ "$typeNumber" != "q" ]; do

    echo "Hello on mini-ERP tools (h to help - q to quite):"

    read -r -p "Type: " typeNumber

    case "$typeNumber" in

    "h")
      function_see_help
      ;;
    "1")
      installing_frontend
      ;;
    "2")
      installing_backend
      ;;
    "3")
      installing_mocha_frontend_test
      ;;
    "4")
      run_frontend_server
      ;;
    "5")
      run_phalcon_server
      ;;
    "6")
      run_mocha_frontend_test
      ;;
    "7")
      run_phalcon
      ;;
    "8")
      run_phalcon_migration_action_run
      ;;
    "9")
      run_phalcon_migration_action_generate
      ;;
    "q") ;;

    *)
      echo "The type don't exist!"
      ;;
    esac
    echo ""
  done
}

############################################################################
################################## FUNCTIONS ###############################
function_see_help() {
  echo "Type h or help in order to see all commands"
  echo "Type 1 installing frontend (npm install)"
  echo "Type 2 installing backend (composer install)"
  echo "Type 3 installing Mocha fronted test (npn install)"
  echo "Type 4 run frontend (npm run serve)"
  echo "Type 5 run backend (phalcon serve)"
  echo "Type 6 run Mocha fronted test (npm run test)"
  echo "Type 7 run phalcon commands (phalcon)"
  echo "Type 8 generating the db by migration (phalcon migration --action=run)"
  echo "Type 9 generating the php file from database (phalcon migration --action=generate)"

}

## Installing section
installing_backend() {
  cd ./$backendFolder || exit
  composer install
  cd .. || exit
}
installing_frontend() {
  cd ./$frontendFolder || exit
  npm install
  cd .. || exit
}
installing_mocha_frontend_test() {
  cd ./$mochaFrontendTestFolder || exit
  npm install
  cd .. || exit
}

## Running section
run_frontend_server() {
  cd ./$frontendFolder || exit
  npm run serve
  cd .. || exit
}

run_mocha_frontend_test() {
  echo "${red}The frontend and backend server must run otherwise all the test will fails${reset}"
  cd ./$mochaFrontendTestFolder || exit
  npm run test
  cd .. || exit
}

run_phalcon_migration_action_run() {
  cd ./$backendFolder || exit
  phalcon migration --action=run
  cd .. || exit
}

run_phalcon_migration_action_generate() {
  cd ./$backendFolder || exit
  phalcon migration --action=generate
  cd .. || exit
}

run_phalcon_server() {
  cd ./$backendFolder || exit
  phalcon serve
  cd .. || exit
}

## Others
run_phalcon() {
  phalcon
}

############################################################################
################################## START SCRIPT ############################
run_main
