
runtests() {
  echo "running the tests"
  mocha --timeout 60000 --exit --reporter mochawesome --reporter-options reportDir=results,reportFilename=result
}

showresults() {
  echo "showing results"
  if [[ "$OSTYPE" == "darwin"* ]]; then
    # Mac OSX
    open results/result.html
  else
    # Maybe not necessary on other platforms
    echo "the showing feature is no implemented on this platform"
  fi
}

runtests
# showresults