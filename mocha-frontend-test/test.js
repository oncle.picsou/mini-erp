const chai = require('chai')
const chaiHttp = require('chai-http')
const { By, until } = require('selenium-webdriver')

const { Builder } = require('selenium-webdriver');
const { Options } = require('selenium-webdriver/chrome');

chai.use(chaiHttp);
chai.should();

//The host of front end server
let host = 'http://localhost:8080'
let chromeDriver

/**
 *
 * @param params made of {driver, id, timeout, msg}
 * @returns {Promise<*>}
 */
async function waitForElementById(params) {
    let {driver, id, timeout, msg} = params
    try {
        if (timeout === undefined) timeout = 5000
        return await driver.wait(until.elementLocated(By.css('#' + id)), timeout)
    } catch (e) {
        chai.assert(false, msg)
    }
}

async function buildChromeDriver() {
    let _wDriver = await new Builder().forBrowser('chrome').build();
    await _wDriver.get(host);
    _wDriver.waitForElementById = waitForElementById // custom
    return _wDriver
}


before(async () => {
    console.log('Creating driver')
    try {
        chromeDriver = await buildChromeDriver()
        console.log('Driver built')
    } catch (e) {
        console.error(`Problem, the driver has not been created ${e.toString()}`)
    }
})

describe('Login tests', () => {
    it('Login modal structure', async () => {
        let loginButton = await chromeDriver.waitForElementById({
            driver:chromeDriver, id:'navLoginButton', msg:`It is not possible to find the login button`})

        let loginButtonTagName = await loginButton.getTagName()
        chai.assert(loginButtonTagName === 'button', `The element must be a button but it was ${loginButtonTagName}`)

        await loginButton.click()

        let modalForm = await chromeDriver.waitForElementById({
            driver:chromeDriver, id:'loginModalId', msg:`It is not possible to get the login modal`})

        let modalFormTagName = await modalForm.getTagName()
        chai.assert(modalFormTagName === 'div', `The element must be a b-modal ma was ${modalFormTagName}`)

    })
    it('Insert the data on the form', async () => {
        let doLoginButton = await chromeDriver.waitForElementById({
            driver:chromeDriver, id:'loginSubmitButtonId', msg:`Cannot get the login button`})

        let usernameInput = await chromeDriver.waitForElementById({
            driver:chromeDriver, id:'loginUserInputId', msg:`Cannot get the user input field`})

        let passwordInput = await chromeDriver.waitForElementById({
            driver:chromeDriver, id:'loginPasswordInputId', msg:`Cannot get the password input field`})

        // ... chai asserts

        await usernameInput.sendKeys('test')
        await passwordInput.sendKeys('test')
        await doLoginButton.click()

        // ... chai assert login success
    })
})
/*
describe('Gruppo  di test 2', () => {
    console.log('inizio gruppo di test 2')
    it('esempio di test sincrono', (done) => {
        console.log('avvio test sincrono')
        let a = 5
        chai.assert(a === 4, `La variabile a doveva essere 4 ma era ${a}`)
        done()
    })
    it('esempio di test asincrono', async () => {
        console.log('avvio test asincrono')
        let a = 5
        await wait()
        chai.assert(a === 5, `La variabile a doveva essere 5 ma era ${a}`)

    })
})

describe('Conclusione test e chiusura browser', () => {
    it('Chiudo browser gracefully', async () => {
        try {
            console.log('closing')
            await chromeDriver.close()
            await chromeDriver.quit()
            console.log('closed')
        } catch (e) {
            console.error('cannot close browser')
            console.error(e)
            throw new Error('Cannot close the browser at the end of the tests: ' + e.toString())
        }
    })
})
*/
let wait = async () => new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve()
    }, 2500)
})
