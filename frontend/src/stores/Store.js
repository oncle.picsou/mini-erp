import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

/**
 * The idea of the store is to centralize all the information in an unique big bag in order to inform in the same time
 * all the components using the subscription. For instance, the navbar has the job to do the login and logout, but if
 * this state changes, the view of table must change. So the navbar has to propagate this information. mini-ERP uses this
 * state in order to do it.
 *
 * This state is not only used for the change of user state but even in order to store the information when there is
 * a retrieving from another table: the users wishes to set for a transaction some products, so has to pass from a table
 * to another table without to lose the information.
 */
const state = {
    user: {
        username: '', //Not yet used
        isLogged: false,
        isEditing: false,

        //Retrieving elements
        countVisit: -1,
        matrix: null,
        isGetOne: true,
        isRetrieving: false,

        //Contains all the information from which table (row and element) the user wishes to retrieve the information.
        nameOfTable: null,
        rowTableId: null,
        elementOfTable: null,

        //The name of the table that wishes to retrieve the items for instance: companyClient table wishes to retrieve
        //some information.
        retrievingByTable: null,

        //The elements to retrieve (a vector in order to be coherent of future table that has more than one elements.
        retrievedIds: [],

        //Contains the elements about all the recoveries
        retrieveElements: [{
            //Table, row and element for which the user wants to retrieve information
            nameOfTable: null,  //Name of the table for which the user wants to retrieve information (table)
            rowTableId: null,  //The id row for which the user wants to retrieve information (row)
            elementOfTable: null,  //The element of the table for which the user wants to retrieve information (element)
            //The elements to retrieve (a vector in order to be coherent of future table that has more than one elements.
            //TODO: to think if it is better to cut and to use only numbers element.
            retrievedIds: [],
            //Used only for the retrieving of product because there is also the number of product to get.
            numbers: [{
                id: null, //Id of product
                number: 0 //Number of product that the user wishes to get
            }]
        }]
    }
}

const mutations = {
    destroyRetrieve: (state) => {
        state.user.matrix = null;
        state.user.countVisit = -1;
        state.user.isGetOne = true;
        state.user.isRetrieving = false;
        state.user.nameOfTable = null;
        state.user.rowTableId = null;
        state.user.elementOfTable = null;
        state.user.retrievingByTable = null;
        state.user.retrievedIds = [];
        state.user.retrieveElements = [{
            nameOfTable: null,
            rowTableId: null,
            elementOfTable: null,
            retrievedIds: [],
            numbers: [{
                id: null,
                number: 0
            }]
        }];
    },
    setCountVisitByStore: (state, countVisit) => {
        state.user.countVisit = countVisit;
    },
    setIsGetOneByStore: (state, isGetOne) => {
        state.user.isGetOne = isGetOne;
    },
    setRetrieveByStore: (state, payload) => {
        let index = state.user.retrieveElements.findIndex((
            row) =>
            row.nameOfTable === payload.nameOfTable &&
            row.rowTableId === payload.rowTableId &&
            row.elementOfTable === payload.elementOfTable
        );
        if (payload.isRetrieving === true) {
            if (index >= 0) {
                state.user.retrievedIds = JSON.parse(JSON.stringify(state.user.retrieveElements[index].retrievedIds));
            } else {
                state.user.retrievedIds = [];
                state.user.retrieveElements.push({

                    nameOfTable: payload.nameOfTable,
                    rowTableId: payload.rowTableId,
                    elementOfTable: payload.elementOfTable,

                    retrievedIds: [],
                    numbers: [{
                        id: null,
                        number: 0
                    }]
                });
            }
            state.user.matrix = JSON.parse(JSON.stringify(payload.matrix));
            state.user.countVisit = payload.countVisit;
            state.user.isGetOne = payload.isGetOne;
            state.user.isRetrieving = payload.isRetrieving;

            state.user.nameOfTable = payload.nameOfTable;
            state.user.elementOfTable = payload.elementOfTable
            state.user.rowTableId = payload.rowTableId;

            state.user.retrievingByTable = payload.retrievingByTable;
        } else {
            state.user.retrieveElements[index].retrievedIds =
                JSON.parse(JSON.stringify(payload.retrievedIds));
            state.user.retrieveElements[index].numbers = JSON.parse(JSON.stringify(payload.numbers));
            state.user.retrievedIds = [];
            state.user.isRetrieving = false;
        }
    },
    setUserByStoreAsLogged: (state, name) => {
        state.user.username = name;
        state.user.isLogged = true;
    },
    setUserByStoreAsLogout: (state) => {
        state.user.username = null;
        state.user.isLogged = false;
        state.user.isEditing = false;
    },
    /**
     * The goal of this mutation is not only to set if the user is editing a table or not, but also to prevent that
     * the user loses some modifications. TODO: the goal is not attempted very well.
     *
     * @param state
     * @param isEditing
     */
    setUserByStoreIsEditing: (state, isEditing) => {
        state.user.isEditing = isEditing;
    }
}

const actions = {}

/**
 * This constants contains the getters of the store.
 * There is only one get in order to be easier to access to the story (it is not maybe a perfect solution).
 */
const getters = {
    getUserByStore: (state) => {
        return state.user
    }
}

/**
 * It is the store. The wrapper has been used in order maybe (if it is possible) to split on more files: each file will
 * contain an item (like for instance: file A only the mutations etc...).
 */
export default new Vuex.Store({
    actions: actions,
    getters: getters,
    mutations: mutations,
    state: state,
    strict: true
});

