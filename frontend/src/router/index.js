import Vue from 'vue'
import Router from 'vue-router'

import Home from '../views/Home.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/client',
            name: 'client',
            component: () => import('../views/Table.vue')
        },
        {
            path: '/company',
            name: 'company',
            component: () => import('../views/Table.vue')
        },
        {
            path: '/employee',
            name: 'employee',
            component: () => import('../views/Table.vue')
        },
        {
            path: '/product',
            name: 'product',
            component: () => import('../views/Table.vue')
        },
        {
            path: '/provider',
            name: 'provider',
            component: () => import('../views/Table.vue')
        }, //Transactions
        {
            path: '/companyClient',
            name: 'companyClient',
            component: () => import('../views/Table.vue')
        },
        /* Not yet implemented
        {
            path: '/providerCompany',
            name: 'providerCompany',
            component: () => import('../views/Table.vue')
        },*/
        {
            path: '*',
            redirect: '/'
        }
    ]
})