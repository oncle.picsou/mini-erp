import Vue from 'vue'
import Vuex from 'vuex';
import VueSweetalert2 from 'vue-sweetalert2';
import VueResource  from 'vue-resource';

import App from './App.vue'
import router from './router'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.config.productionTip = false

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueSweetalert2);
Vue.use(VueResource);
Vue.use(Vuex);


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')


